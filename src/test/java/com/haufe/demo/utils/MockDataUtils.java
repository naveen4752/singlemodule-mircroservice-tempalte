package com.haufe.demo.utils;

import java.util.Date;
import java.util.Optional;

import com.haufe.demo.domain.AppDomain;

public class MockDataUtils {

	public static Optional<AppDomain> prepareAppDomain(long id) {
		AppDomain appDomain = new AppDomain();
		appDomain.setId(1l);
		appDomain.setCreatedDate(new Date());
		appDomain.setName("john_" + System.currentTimeMillis());
		return Optional.of(appDomain);
	}
}
