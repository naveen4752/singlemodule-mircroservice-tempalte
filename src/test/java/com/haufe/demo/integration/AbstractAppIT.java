package com.haufe.demo.integration;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.haufe.demo.SpringbootApplication;

@SpringBootTest(classes = SpringbootApplication.class)
@RunWith(SpringRunner.class)
public abstract class AbstractAppIT {

}
