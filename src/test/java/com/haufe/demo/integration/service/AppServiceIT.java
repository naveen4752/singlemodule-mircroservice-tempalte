package com.haufe.demo.integration.service;

import java.text.ParseException;
import java.util.Date;

import javax.persistence.EntityNotFoundException;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.haufe.demo.integration.AbstractAppIT;
import com.haufe.demo.service.AppService;
import com.haufe.demo.utils.DateUtils;
import com.haufe.demo.web.dto.AppDTO;

public class AppServiceIT extends AbstractAppIT {

	@Autowired
	private AppService appService;

	@Test
	public void when_entity_exists_then_getAppByIdTest() {
		Assert.assertNotNull(appService.getApp(1l));
	}
	
	@Test
	public void when_entity_exists_then_getAppsTest() throws ParseException {
		Assert.assertNotNull(appService.getApps());
	}

	@Test(expected = EntityNotFoundException.class)
	public void when_entity_doesnot_exist_then_getAppByIdTest() {
		appService.getApp(11l);
	}

	@Test
	public void deleteAppByIdTest() {
		appService.deleteApp(1l);
	}

	@Test
	public void createAppest() throws ParseException {
		AppDTO appDTO = new AppDTO();
		appDTO.setCreatedDate(DateUtils.convertDateIntoString(new Date(System.currentTimeMillis())));
		appDTO.setName("john_" + System.currentTimeMillis());
		appService.createApp(appDTO);
	}

}
