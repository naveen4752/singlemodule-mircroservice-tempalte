package com.haufe.demo.repository.stream;

import java.io.Serializable;
import java.util.stream.Stream;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface StreamingRepository<T, ID extends Serializable> extends JpaRepository<T, ID> {

	Stream<T> streamAll();
}
