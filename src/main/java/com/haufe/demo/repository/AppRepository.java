package com.haufe.demo.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.haufe.demo.domain.AppDomain;

public interface AppRepository extends JpaRepository<AppDomain, Serializable> {

}
