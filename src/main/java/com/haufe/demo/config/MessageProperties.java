package com.haufe.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.haufe.demo.web.dto.messages.HttpMessages;

import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties
@PropertySource("classpath:messages.properties")
@Getter
@Setter
public class MessageProperties {
	private HttpMessages http;
	
	public MessageProperties() {
		System.out.println("123.....");
	}
}
