package com.haufe.demo.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppDTO {

	private long id;
	@JsonProperty("created_date")
	private String createdDate;
	private String name;
}
