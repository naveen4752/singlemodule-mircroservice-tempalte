package com.haufe.demo.web.dto.result;

import org.springframework.data.domain.Page;

public abstract class AbstractPageResult<T> extends AbstractResult implements Page<T> {

}
