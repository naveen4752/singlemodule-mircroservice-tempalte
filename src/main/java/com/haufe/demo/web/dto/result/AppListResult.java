package com.haufe.demo.web.dto.result;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.haufe.demo.web.dto.AppDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppListResult extends AbstractResult {

	@JsonProperty("apps")
	private List<AppDTO> appList;
}
