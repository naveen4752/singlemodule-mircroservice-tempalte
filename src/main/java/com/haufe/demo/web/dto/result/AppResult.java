package com.haufe.demo.web.dto.result;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.haufe.demo.web.dto.AppDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppResult extends AbstractResult {

	@JsonProperty("app")
	private AppDTO app;
}
