package com.haufe.demo.web.dto.result;

import org.springframework.hateoas.ResourceSupport;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractResult extends ResourceSupport{
}
