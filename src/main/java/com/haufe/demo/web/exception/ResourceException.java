package com.haufe.demo.web.exception;

public abstract class ResourceException extends Exception{

	public ResourceException(String mess){
		super(mess);
	}
}
