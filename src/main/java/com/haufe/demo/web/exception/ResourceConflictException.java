package com.haufe.demo.web.exception;

public class ResourceConflictException extends ResourceException {
	public ResourceConflictException(String mess) {
		super(mess);
	}
}
