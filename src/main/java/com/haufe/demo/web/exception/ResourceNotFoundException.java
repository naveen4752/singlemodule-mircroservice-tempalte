package com.haufe.demo.web.exception;

import lombok.Getter;

@Getter
public class ResourceNotFoundException extends ResourceException {

	private String mess;
	
	public ResourceNotFoundException(String mess) {
		super(mess);
	}
}
