package com.haufe.demo.web.exception.handler;

import com.haufe.demo.web.exception.ResourceException;

public class BadRequestException extends ResourceException {
	public BadRequestException(String mess) {
		super(mess);
	}
}
