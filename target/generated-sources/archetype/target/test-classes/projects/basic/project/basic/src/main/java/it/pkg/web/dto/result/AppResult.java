package it.pkg.web.dto.result;

import com.fasterxml.jackson.annotation.JsonProperty;
import it.pkg.web.dto.AppDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppResult extends AbstractResult {

	@JsonProperty("app")
	private AppDTO app;
}
