package it.pkg.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import it.pkg.domain.AppDomain;

public interface AppRepository extends JpaRepository<AppDomain, Serializable> {

}
