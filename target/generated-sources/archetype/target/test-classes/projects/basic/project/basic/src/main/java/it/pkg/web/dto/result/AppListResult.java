package it.pkg.web.dto.result;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import it.pkg.web.dto.AppDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppListResult extends AbstractResult {

	@JsonProperty("apps")
	private List<AppDTO> appList;
}
