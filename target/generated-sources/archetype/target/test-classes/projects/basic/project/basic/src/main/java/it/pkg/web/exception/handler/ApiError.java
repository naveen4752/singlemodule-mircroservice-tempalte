package it.pkg.web.exception.handler;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiError {
	@JsonIgnore
	private HttpStatus httpStatus;
	
	@JsonProperty("status")
	private int status;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	private LocalDateTime timestamp;
	private String message;
	private String debugMessage;
	private List<ApiSubError> subErrors;
	private String path;
	

	private ApiError() {
		timestamp = LocalDateTime.now();
	}

	ApiError(HttpStatus httpStatus) {
		this();
		this.httpStatus = httpStatus;
		this.status = httpStatus.value();
	}

	ApiError(HttpStatus httpStatus, Throwable ex) {
		this();
		this.httpStatus = httpStatus;
		this.message = "Unexpected error";
		this.debugMessage = ex.getLocalizedMessage();
		this.status = httpStatus.value();
	}

	ApiError(HttpStatus httpStatus, String message, Throwable ex) {
		this();
		this.httpStatus = httpStatus;
		this.message = message;
		this.debugMessage = ex.getLocalizedMessage();
		this.status = httpStatus.value();
	}
}
