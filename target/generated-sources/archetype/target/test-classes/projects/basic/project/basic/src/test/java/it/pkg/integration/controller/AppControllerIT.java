package it.pkg.integration.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import it.pkg.integration.AbstractAppIT;
import it.pkg.web.controller.AppController;
import it.pkg.web.exception.handler.RestExceptionHandler;

public class AppControllerIT extends AbstractAppIT {

	@Autowired
	private AppController appController;

	private MockMvc mockMvc;
	
	@Autowired
	private WebApplicationContext context;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		List<RestExceptionHandler> list = new ArrayList<>();
		list.add(new RestExceptionHandler());
		mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
	}
	
	/**
	 * When call incorrect URL (404)
	 * @throws Exception
	 */
	
	@Test
	public void when_wrong_url_then_getAppTest() throws Exception {
		mockMvc.perform(get("/appss/{id}", 11l)
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andDo(MockMvcResultHandlers.print()).andExpect(status().isNotFound());
	}
	
	/**
	 * when no resource existing with given identifier (404)
	 * @throws Exception
	 */
	@Test(expected=Exception.class)
	public void when_resource_not_exists_then_getAppTest() throws Exception {
		MvcResult result = (MvcResult) mockMvc.perform(get("/apps/{id}", 11l)
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andDo(MockMvcResultHandlers.print()).andExpect(status().isNotFound());
		Assert.assertTrue(result.getResponse().getStatus() == HttpStatus.NOT_FOUND.value());
	}
	
	/**
	 * when accept not as expected (406) 
	 * @throws Exception
	 */
	@Test
	public void when_accept_type_not_correct_then_getAppTest() throws Exception {
		mockMvc.perform(get("/apps/{id}", 11l)
				.accept(MediaType.APPLICATION_ATOM_XML_VALUE))
				.andDo(MockMvcResultHandlers.print()).andExpect(status().isNotAcceptable());
	}
	
	/**
	 * when method not as expected (415)
	 * @throws Exception
	 */
	@Test
	public void when_method_not_correct_then_getAppTest() throws Exception {
		mockMvc.perform(post("/apps/{id}", 11l)
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andDo(MockMvcResultHandlers.print()).andExpect(status().isMethodNotAllowed());
	}
	
	/**
	 * when correct httprequest
	 * @throws Exception
	 */
	@Test
	public void getAppTest() throws Exception {
		mockMvc.perform(get("/apps/{id}", 1l)
				.content(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andDo(MockMvcResultHandlers.print()).andExpect(status().isOk());
	}
	
}
