package it.pkg.web.dto.messages;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HttpMessages {

	private String resourceNotFound;
	private String methodNotAllowed;
	private String incorrectConentType;
	private String incorrectAcceptType;
	private String resourceConflict;
}
