package it.pkg.web.exception.handler;

import it.pkg.web.exception.ResourceException;

public class BadRequestException extends ResourceException {
	public BadRequestException(String mess) {
		super(mess);
	}
}
