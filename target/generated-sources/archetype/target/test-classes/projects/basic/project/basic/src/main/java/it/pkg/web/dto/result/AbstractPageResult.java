package it.pkg.web.dto.result;

import org.springframework.data.domain.Page;

public abstract class AbstractPageResult<T> extends AbstractResult implements Page<T> {

}
