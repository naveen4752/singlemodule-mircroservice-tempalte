package it.pkg.service;

import static org.mockito.Mockito.when;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import it.pkg.repository.AppRepository;
import it.pkg.utils.MockDataUtils;

@RunWith(SpringRunner.class)
public class AppServiceTest {

	@InjectMocks
	private AppService appService;

	@Mock
	private AppRepository appRepository;

	@Test(expected=EntityNotFoundException.class)
	public void whenAppDomain_null_then_getAppTest() {
		when(appRepository.findById(1l)).thenReturn(Optional.ofNullable(null));
		appService.getApp(1l);
	}

	@Test
	public void getAppTest() {
		when(appRepository.findById(1l)).thenReturn(MockDataUtils.prepareAppDomain(1l));
		Assert.assertNotNull(appService.getApp(1l));
	}
}
