#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ${package}.domain.AppDomain;
import ${package}.repository.AppRepository;
import ${package}.repository.AppStreamRepository;
import ${package}.utils.DateUtils;
import ${package}.web.dto.AppDTO;

@Service
@Transactional
public class AppService {

	@Autowired
	private AppRepository appRepository;

	@Autowired
	private AppStreamRepository appStreamRepository;

	public AppDTO getApp(long id) throws EntityNotFoundException {
		AppDomain appDomain = null;
		try {
			Optional<AppDomain> domain = appRepository.findById(id);
			appDomain = domain.get();
		} catch (NoSuchElementException e) {
			throw new EntityNotFoundException("No App found with id :" + id);
		}
		return convertDomainToDto(appDomain);
	}

	public List<AppDTO> getApps() throws ParseException {
		List<AppDTO> list = new ArrayList<>();
		appStreamRepository.streamAll().forEach(itm -> {
			list.add(convertDomainToDto(itm));
		});

		return list;
	}

	@Transactional
	public AppDTO createApp(AppDTO appDTO) throws ParseException {
		AppDomain appDomain = appRepository.save(convertDTOToDomain(appDTO));
		return convertDomainToDto(appDomain);
	}

	public boolean deleteApp(Long id) throws EntityNotFoundException {
		try {
			appRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new EntityNotFoundException("No App exist with given Id : " + id);
		}

		return true;
	}

	private AppDTO convertDomainToDto(AppDomain appDomain) {
		AppDTO appDTO = new AppDTO();
		appDTO.setId(appDomain.getId());
		appDTO.setCreatedDate(DateUtils.convertDateIntoString(appDomain.getCreatedDate()));
		appDTO.setName(appDomain.getName());
		return appDTO;
	}

	private AppDomain convertDTOToDomain(AppDTO appDTO) throws ParseException {
		if (null == appDTO) {
			return null;
		}

		AppDomain appDomain = new AppDomain();
		appDomain.setId(appDTO.getId());
		appDomain.setCreatedDate(DateUtils.convertStringIntoDate(appDTO.getCreatedDate()));
		appDomain.setName(appDTO.getName());
		return appDomain;
	}
}
