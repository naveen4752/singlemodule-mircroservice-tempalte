#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.web.dto.messages;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HttpMessages {

	private String resourceNotFound;
	private String methodNotAllowed;
	private String incorrectConentType;
	private String incorrectAcceptType;
	private String resourceConflict;
}
