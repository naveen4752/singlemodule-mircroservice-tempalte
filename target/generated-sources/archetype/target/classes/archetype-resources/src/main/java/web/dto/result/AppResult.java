#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.web.dto.result;

import com.fasterxml.jackson.annotation.JsonProperty;
import ${package}.web.dto.AppDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppResult extends AbstractResult {

	@JsonProperty("app")
	private AppDTO app;
}
