drop table if exists app_domain;

CREATE TABLE if not exists app_domain (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  created_date TIMESTAMP NOT NULL,
  name varchar  NOT NULL
);

INSERT INTO app_domain (id, created_date, name) VALUES (1, now(), 'customer');
INSERT INTO app_domain (created_date, name) VALUES (now(), 'order');
INSERT INTO app_domain (created_date, name) VALUES (now(), 'product');