#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.web.exception;

public abstract class ResourceException extends Exception{

	public ResourceException(String mess){
		super(mess);
	}
}
