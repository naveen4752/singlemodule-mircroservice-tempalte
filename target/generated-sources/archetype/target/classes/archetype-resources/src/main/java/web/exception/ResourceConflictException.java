#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.web.exception;

public class ResourceConflictException extends ResourceException {
	public ResourceConflictException(String mess) {
		super(mess);
	}
}
