#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import ${package}.web.dto.messages.HttpMessages;

import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties
@PropertySource("classpath:messages.properties")
@Getter
@Setter
public class MessageProperties {
	private HttpMessages http;
	
	public MessageProperties() {
		System.out.println("123.....");
	}
}
