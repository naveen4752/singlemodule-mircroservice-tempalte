#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.repository;

import static org.hibernate.jpa.QueryHints.HINT_FETCH_SIZE;

import java.io.Serializable;
import java.util.stream.Stream;

import javax.persistence.QueryHint;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;

import ${package}.domain.AppDomain;
import ${package}.repository.stream.StreamingRepository;

public interface AppStreamRepository extends StreamingRepository<AppDomain, Serializable> {

	@Query(value = "select a from AppDomain a")
	@QueryHints({ @QueryHint(name = HINT_FETCH_SIZE, value = "10") })
	Stream<AppDomain> streamAll();
}
