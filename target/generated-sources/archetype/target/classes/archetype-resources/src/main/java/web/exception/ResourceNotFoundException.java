#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.web.exception;

import lombok.Getter;

@Getter
public class ResourceNotFoundException extends ResourceException {

	private String mess;
	
	public ResourceNotFoundException(String mess) {
		super(mess);
	}
}
