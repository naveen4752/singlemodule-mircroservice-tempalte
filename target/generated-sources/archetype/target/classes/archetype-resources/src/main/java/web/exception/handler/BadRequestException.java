#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.web.exception.handler;

import ${package}.web.exception.ResourceException;

public class BadRequestException extends ResourceException {
	public BadRequestException(String mess) {
		super(mess);
	}
}
