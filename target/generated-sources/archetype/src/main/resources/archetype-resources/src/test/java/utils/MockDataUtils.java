#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.utils;

import java.util.Date;
import java.util.Optional;

import ${package}.domain.AppDomain;

public class MockDataUtils {

	public static Optional<AppDomain> prepareAppDomain(long id) {
		AppDomain appDomain = new AppDomain();
		appDomain.setId(1l);
		appDomain.setCreatedDate(new Date());
		appDomain.setName("john_" + System.currentTimeMillis());
		return Optional.of(appDomain);
	}
}
