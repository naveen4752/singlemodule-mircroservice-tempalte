#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import ${package}.domain.AppDomain;

public interface AppRepository extends JpaRepository<AppDomain, Serializable> {

}
