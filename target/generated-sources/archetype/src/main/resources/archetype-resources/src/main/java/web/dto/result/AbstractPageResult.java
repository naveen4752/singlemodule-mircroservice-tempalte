#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.web.dto.result;

import org.springframework.data.domain.Page;

public abstract class AbstractPageResult<T> extends AbstractResult implements Page<T> {

}
