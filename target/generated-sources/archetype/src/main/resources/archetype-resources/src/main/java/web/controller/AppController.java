#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.web.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

import java.text.ParseException;

import javax.persistence.EntityNotFoundException;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ${package}.config.MessageProperties;
import ${package}.service.AppService;
import ${package}.web.dto.result.AppListResult;
import ${package}.web.dto.result.AppResult;
import ${package}.web.exception.ResourceNotFoundException;
import ${package}.web.exception.handler.BadRequestException;

@RestController
@RequestMapping("/apps")
public class AppController {

	@Autowired
	private AppService appService;
	
	@Autowired
	private MessageProperties messProp;

	@GetMapping
	public ResponseEntity<AppListResult> getApps() throws BadRequestException {
		AppListResult appListResult = new AppListResult();
		try {
			
			appListResult.setAppList(appService.getApps());
		} catch (ParseException e) {
			throw new BadRequestException("Malformed Exception !");
		}
		appListResult.add(linkTo(AppController.class).withSelfRel());
		return new ResponseEntity<AppListResult>(appListResult, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}", produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AppResult> getApp(@PathVariable(name = "id", required = true) long id)
			throws ResourceNotFoundException {
		
		AppResult appResult = new AppResult();
		try {
			appResult.setApp(appService.getApp(id));
		} catch (EntityNotFoundException e) {
			throw new ResourceNotFoundException(messProp.getHttp().getResourceNotFound() + " :" + id);
		}

		appResult.add(linkTo(AppController.class).slash(id).withSelfRel());
		return new ResponseEntity<AppResult>(appResult, HttpStatus.OK);
	}


	@PostMapping("/")
	public ResponseEntity<AppListResult> createApp(@PathParam("id") long id) throws BadRequestException {
		AppListResult appListResult = new AppListResult();
		try {
			appListResult.setAppList(appService.getApps());
		} catch (ParseException e) {
			throw new BadRequestException("Malformed Request!");
		}
		return new ResponseEntity<AppListResult>(appListResult, HttpStatus.OK);
	}

	@PutMapping("/")
	public ResponseEntity<AppListResult> updateApp(@PathParam("id") long id) throws BadRequestException {
		AppListResult appListResult = new AppListResult();
		try {
			appListResult.setAppList(appService.getApps());
		} catch (ParseException e) {
			throw new BadRequestException("Malformed Request!");
		}
		return new ResponseEntity<AppListResult>(appListResult, HttpStatus.OK);
	}

	@PatchMapping("/")
	public ResponseEntity<AppListResult> updatePatchApp(@PathParam("id") long id) {
		AppListResult appListResult = new AppListResult();
		try {
			appListResult.setAppList(appService.getApps());
		} catch (ParseException e) {
		}
		return new ResponseEntity<AppListResult>(appListResult, HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<AppListResult> deleteApp(@PathVariable(name = "id", required = true) long id) throws ResourceNotFoundException {
		try {
		appService.deleteApp(id);
		} catch(EntityNotFoundException e) {
			throw new ResourceNotFoundException("No App found with given id: "+ id);
		}
		return new ResponseEntity<AppListResult>(HttpStatus.OK);
	}

}
