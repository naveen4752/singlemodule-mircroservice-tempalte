#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.integration;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import ${package}.SpringbootApplication;

@SpringBootTest(classes = SpringbootApplication.class)
@RunWith(SpringRunner.class)
public abstract class AbstractAppIT {

}
