#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.web.dto.result;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import ${package}.web.dto.AppDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppListResult extends AbstractResult {

	@JsonProperty("apps")
	private List<AppDTO> appList;
}
